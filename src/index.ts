import express from "express";
import socketio from "socket.io";
import http from "http";
import path from "path";

const app = express();
const httpServer = http.createServer(app);
const io = new socketio.Server(httpServer);

app.use(express.static(path.resolve(__dirname, "..", "public")));
let sub = 0;
io.on("connection", (socket) => {
  console.log(`New Connection: ${socket.id}`);

  socket.on("message", (message) => {
    console.log(`New Message ${message}`);

    io.emit('received', {status: 'ok', message: `Received message ${message}`});
  });

  socket.on("newPoint", point => {
      let pointsTotal = sub+=point;
      console.log(`Points ${pointsTotal}`)
      io.emit("createPoint", pointsTotal);
  });
//   io.on ou socket.broadcast => Envia para todos.
});

httpServer.listen(3333, () => {
  console.log("Server Running");
});
